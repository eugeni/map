package org.useful.search

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.settings.ImmutableSettings
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress

class ElasticSearchClientFactory {
    GrailsApplication grailsApplication

    private Client elasticSearchClient

    public Client getElasticSearchClient() {
        if (this.elasticSearchClient) return this.elasticSearchClient

        String clusterName = grailsApplication.config.up.elasticsearch.cluster.name
        Map transportAddresses = grailsApplication.config.up.elasticsearch.transport.addresses

        Settings settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", clusterName).build()
        this.elasticSearchClient = new TransportClient(settings)
        if (transportAddresses) {
            transportAddresses.each { String host, Integer port ->
                this.elasticSearchClient.addTransportAddress(new InetSocketTransportAddress(host, port))
            }
        }

        return this.elasticSearchClient
    }
}
