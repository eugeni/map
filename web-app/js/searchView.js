define('searchView', ['backbone', 'app.logger', 'googleMapAPI', 'map'],
    function(Backbone, log, google, map) {
        log = log.get('searchView');

        var SearchView = Backbone.View.extend({
            el: Backbone.$('#search-form'),
            events: {
                "click #searchButton": "doSearch",
                "click #resetButton": "doReset",
                "click #getCenterPointButton":  "getPoint"
            },
            initialize: function() {
                this.inputSearch = this.$('[name="search"]');
                this.inputLat = this.$('[name="searched-lat"]');
                this.inputLong = this.$('[name="searched-long"]');
                this.inputRad = this.$('[name="searched-rad"]');
            },
            doSearch: function() {
                log.debug('doSearch');
                this.collection.setSearchParams({
                    searchText: this.inputSearch.val(),
                    searchLat: this.inputLat.val(),
                    searchLong: this.inputLong.val(),
                    searchRad: this.inputRad.val()
                });

                this.collection.goTo(0, {remove: true});
            },
            doReset: function() {
                this.inputSearch.val('');
                this.inputLat.val('');
                this.inputLong.val('');
                this.inputRad.val('');
            },
            getPoint: function() {
                var self = this;
                var thisMapsEventListener = google.maps.event.addListener(map, 'click', function(coord) {
                    self.inputLat.val(coord.latLng.jb);
                    self.inputLong.val(coord.latLng.kb);
                    google.maps.event.removeListener(thisMapsEventListener);
                    log.debug('Clicked on map');
                });
            }
        });

        log.debug('SearchView');

        return SearchView;
    }
);
