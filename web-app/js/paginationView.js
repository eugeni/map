define('paginationView', ['backbone', 'mustache', 'app.logger'],
function(Backbone, Mustache, log) {
    log = log.get('paginationView');

    var PaginationView = Backbone.View.extend({
        el: Backbone.$('#pagination'),
        template: Mustache.compile(Backbone.$('#paginationTmpl').html()),
        events: {
            'click a': 'goToPage'
        },
        initialize: function() {
            this.listenTo(this.collection, 'sync', this.rebuild);
        },
        rebuild: function() {
            var info = this.collection.info();
            var pagination = [];
            for (var i = 0; i < info.totalPages; i ++) {
                var page = {n: i + 1};
                if (i == info.currentPage) {
                    page['current'] = 'current';
                }
                pagination.push(page);
            }

            this.$el.html(this.template({pagination: pagination}));
        },
        goToPage: function(e) {
            e.preventDefault();
            var page = $(e.target).text();
            page = parseFloat(page);
            if (page) page -=1;
            this.collection.goTo(page);
        }
    });

    log.debug('PaginationView')

    return PaginationView;
});
