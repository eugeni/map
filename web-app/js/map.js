define('map', ['googleMapAPI', 'app.logger', 'jquery'],
    function(google, log, $) {
        log = log.get('map');

        var mapOptions = {
            center: new google.maps.LatLng(49, 32),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

        log.debug('map');

        return map;
    }
);
