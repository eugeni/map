define('place/collection', ['backbone', 'app.logger', 'place/model', 'backbone.paginator'],
    function(Backbone, log, Place) {
        log = log.get('place/collection');

        var PlaceList = Backbone.Paginator.requestPager.extend({
            model: Place,

            paginator_core: {
                type: 'GET',
                dataType: 'json',
                url: '/useful-places/API/v1/place/'
            },
            paginator_ui: {
                firstPage: 0,
                currentPage: 0,
                perPage: 2,
                totalPages: 0
            },
            server_api: {
                'max': function() { return this.perPage; },
                'offset': function() { return (this.perPage * this.currentPage); }
            },
            parse: function (response) {
                log.debug("Paginator function 'parse'");

                this.totalRecords = response.total;

                return response.data;
            },
            setSearchParams: function(searchParams) {
                for(var key in searchParams) {
                    this.server_api[key] = searchParams[key];
                }
            }
        });

        log.debug('Place list');

        return PlaceList;
    }
);
