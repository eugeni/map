define(['backbone', 'app.logger', 'place/model', 'place/collection',
    'place/view', 'place/crudView', 'paginationView', 'searchView'],
    function(Backbone, log, Place, PlaceList,
             PlaceView, AppView, PaginationView, SearchView) {
        log = log.get('place/main');

        var places = new PlaceList();

        var app = new AppView({
            collection: places,
            Place: Place,
            PlaceView: PlaceView
        });

        var paginationView = new PaginationView({
            collection: places
        });

        var searchView = new SearchView({
            collection: places
        });

        log.debug('App initiated');

        return app;
    }
);
