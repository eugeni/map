define('place/model', ['backbone', 'app.logger'],
    function(Backbone, log) {
        log = log.get('place/model');

        var Place = Backbone.Model.extend({
            defaults: function() {
                return {
                    name: "new Place"
                };
            },
            validate: function(attrs, options) {
                if (!attrs.name || !attrs.long || !attrs.lat) {
                    log.debug('place.model is not valid');

                    return "Some of parameters is empty!";
                }
            }
        });

        log.debug('Place model');

        return Place;
    }
);
