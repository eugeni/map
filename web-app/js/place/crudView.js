define('place/crudView', ['backbone', 'app.logger', 'googleMapAPI', 'map'],
    function(Backbone, log, google, map) {
        log = log.get('place/crudView');

        var CrudView = Backbone.View.extend({
            el: Backbone.$('.useful-places'),
            events: {
                "click #saveButton": "savePlace",
                "click #resetButton": "reset",
                "keypress #name":  "saveOnEnter",
                "keypress #description":  "saveOnEnter",
                "keypress #long":  "saveOnEnter",
                "keypress #lat":  "saveOnEnter",
                "click #refresh":  "refresh",
                "click #getPointButton":  "getPoint"
            },
            initialize: function() {

                this.inputId = this.$('[name="id"]');
                this.inputName = this.$('[name="name"]');
                this.inputDescription = this.$('[name="description"]');
                this.inputLong = this.$('[name="long"]');
                this.inputLat = this.$('[name="lat"]');

                this.listenTo(this.collection, 'add', this.addOne);
                this.listenTo(this.collection, 'reset', this.addAll);

                this.collection.fetch();
            },
            refresh: function() {
                log.debug('reFetch');
                this.collection.fetch();
            },
            savePlace: function() {
                log.debug('savePlace');

                var place = this.collection.findWhere({id: parseFloat(this.inputId.val()) });
                if (!place) {
                    place = new this.options.Place();
                }
                log.info(this.inputDescription.val());
                place.set({
                    name: this.inputName.val(),
                    description: this.inputDescription.val(),
                    long: this.inputLong.val(),
                    lat: this.inputLat.val()
                });

                if (!place.isValid()) {
                    log.debug(place.validationError);
                    return;
                }

                this.collection.add(place, {merge: true});
                place.save({wait: true});

                //clean fields
                this.reset();
            },
            saveOnEnter: function(e) {
                if (e.keyCode != 13) return;

                this.savePlace();
            },
            reset: function() {
                this.inputId.val("");
                this.inputName.val("");
                this.inputDescription.val("");
                this.inputLong.val("");
                this.inputLat.val("");
            },
            addOne: function(place) {
                log.debug('addOne');

                var view = new this.options.PlaceView({model: place, map: map});
                this.$("#placeList").append(view.render().el);
            },
            addAll: function() {
                log.debug('addAll');

                this.collection.each(this.addOne, this);
            },
            getPoint: function() {
                var self = this;
                var thisMapsEventListener = google.maps.event.addListener(map, 'click', function(coord) {
                    self.inputLat.val(coord.latLng.jb);
                    self.inputLong.val(coord.latLng.kb);
                    google.maps.event.removeListener(thisMapsEventListener);
                    log.debug('Clicked on map');
                });
            }
        });


        log.debug('CRUD view');

        return CrudView;
    }
);
