define('place/view', ['backbone', 'app.logger', 'mustache', 'googleMapAPI'],
    function(Backbone, log, Mustache, google) {
        log = log.get('place/view');

        var PlaceView = Backbone.View.extend({
            tagName: 'li',
            template: Mustache.compile(Backbone.$('#placeTmpl').html()),
            events: {
                "click button.destroy": "destroyPlace",
                "dblclick .view"  : "edit"
            },
            marker: null,
            initialize: function() {
                this.listenTo(this.model, 'change', this.render);
                this.listenTo(this.model, 'destroy', this.remove);
                this.listenTo(this.model, 'remove', this.remove);
            },
            render: function() {
                var place = this.model;

                this.$el.html(this.template(place.toJSON()));

                if (!this.marker) {
                    this.marker = new google.maps.Marker({
                        map: this.options.map,
                        title: place.get('name')
                    });
                }
                this.marker.setPosition(new google.maps.LatLng(place.get('lat'), place.get('long')));


                return this;
            },
            destroyPlace: function() {
                this.model.destroy({wait: true});
            },
            edit: function() {
                log.debug('edit');
                log.debug("Place Id: " + this.model.get('id') + ", Title: " + this.model.get('name'));
                log.debug("Description: " + this.model.get('description'));

                Backbone.$('[name="id"]').val(this.model.get('id'));
                Backbone.$('[name="name"]').val(this.model.get('name'));
                Backbone.$('[name="description"]').val(this.model.get('description'));
                Backbone.$('[name="lat"]').val(this.model.get('lat'));
                Backbone.$('[name="long"]').val(this.model.get('long'));
            },
            remove: function(place) {
                if (this.marker) {
                    this.marker.setMap(null);
                }

                //Original Backbone part
                this.$el.remove();
                this.stopListening();
                return this;
            }
        });

        log.debug('Place view');

        return PlaceView;
    }
);
