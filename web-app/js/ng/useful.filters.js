define(['angular'],
    function (angular) {

        var app = angular.module('useful.filters', []);

        app.filter('pageRange', ['Pagination', function(Pagination) {
            return function() {
                var total = Math.ceil(Pagination.total / Pagination.max);

                var res = [];
                for (var i=1; i<=total; i++) {
                    res.push(i);
                }

                return res;
            };
        }]);

        return app;
    }
);
