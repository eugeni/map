define(['angular'],
    function (angular) {
        function RadiusWidget(distance, sizer) {
            this.set('distance', distance);
            this.set('sizer', sizer);

            var circle = new google.maps.Circle({
                strokeWeight: 2
            });

            this.bindTo('bounds', circle);

            circle.bindTo('center', this);
            circle.bindTo('map', this);
            circle.bindTo('radius', this);

            this.addSizer_();
        }

        RadiusWidget.prototype = angular.extend(
            new google.maps.MVCObject(),
            {
                distance_changed: function() {
                    this.set('radius', this.get('distance') * 1000);
                },

                addSizer_: function() {
                    if (!this.sizer) {
                        this.sizer = new google.maps.Marker({
                            draggable: true,
                            title: 'Drag me!'
                        });
                    }

                    this.sizer.bindTo('map', this);
                    this.sizer.bindTo('position', this, 'sizer_position');

                    var me = this;
                    google.maps.event.addListener(this.sizer, 'drag', function() {
                        me.setDistance();
                    });
                },

                center_changed: function() {
                    var bounds = this.get('bounds');

                    if (bounds) {
                        var lng = bounds.getNorthEast().lng();

                        var position = new google.maps.LatLng(this.get('center').lat(), lng);
                        this.set('sizer_position', position);
                    }
                },

                distanceBetweenPoints_: function(p1, p2) {
                    if (!p1 || !p2) {
                        return 0;
                    }

                    var R = 6371; // Radius of the Earth in km
                    var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
                    var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
                    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
                            Math.sin(dLon / 2) * Math.sin(dLon / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = R * c;
                    return d;
                },

                setDistance: function() {
                    var pos = this.get('sizer_position');
                    var center = this.get('center');
                    var distance = this.distanceBetweenPoints_(center, pos);

                    this.set('distance', distance);
                }
            }
        );


        function DistanceWidget(params) {
            if (!params.map) return;

            var map = params.map;
            this.set('map', map);
            this.set('position', map.getCenter());

            var marker = params.center;
            if (!marker) {
                marker = new google.maps.Marker({
                    draggable: true,
                    title: 'Move me!'
                });
            }

            marker.bindTo('map', this);
            marker.bindTo('position', this);

            var radiusWidget = new RadiusWidget(params.distance ? params.distance : 50, params.sizer);
            radiusWidget.bindTo('map', this);
            radiusWidget.bindTo('center', this, 'position');
            this.bindTo('distance', radiusWidget);
            this.bindTo('bounds', radiusWidget);

            this.setDistance = function(newVal) {
                radiusWidget.set('distance', newVal);
                radiusWidget.center_changed();
            };
        }
        DistanceWidget.prototype = new google.maps.MVCObject();

        return DistanceWidget;
    }
);
