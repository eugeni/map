require([
    'angular',
    'ui-map',
    'angular-resource',
    'useful.controllers',
    'useful.filters',
    'useful.routing',
    'useful.services',
    'useful.directives'
],
function (angular) {

    var e = document.getElementById("usefulPlacesMap");
    e.removeAttribute('ng-non-bindable');

    angular.bootstrap(e, [
        'ui.map',
        'ngResource',
        'useful.controllers',
        'useful.filters',
        'useful.routing',
        'useful.services',
        'useful.directives'
    ]);

    return {};
});
