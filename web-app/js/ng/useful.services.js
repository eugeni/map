define(['angular', 'search.point.widget'],
    function (angular, DistanceWidget) {

        var app = angular.module('useful.services', []);

        app.factory('Pagination', function() {
            return {
                total: 1,
                curPage: 1,
                max: 2
            };
        });

        app.factory('places', ['$rootScope', 'Pagination', 'Place', '$resource', '$location', function($rootScope, Pagination, Place, $resource, $location) {
            var Places = function() {
                var self = [];

                var searchValue = {
                    q: '',
                    lat: '',
                    long: '',
                    rad: ''
                };

                Object.defineProperty(self, 'refresh', {
                    value: function() {
                        var me = this;

                        var PlaceListPage = $resource('/useful-places/API/v1/ngPlace');
                        var getPlacesParams = angular.extend({}, me.search, {
                                offset: (Pagination.curPage - 1) * Pagination.max,
                                max: Pagination.max
                            }
                        );
                        PlaceListPage.get(getPlacesParams, function(sResp) {
                            self.splice(0, self.length);

                            for (var placeKey in sResp.data) {
                                self.push(new Place(sResp.data[placeKey]));
                            }
                            Pagination.total = sResp.total ? sResp.total : 1;
                            $rootScope.$broadcast('placesUpdated', self);



                            var link = '#/page/' + Pagination.curPage;
                            if (me.search.lat && me.search.long && me.search.rad) {
                                link += '/' + me.search.lat
                                    + '/' + me.search.long
                                    + '/' + me.search.rad;
                            }
                            if (me.search.q) {
                                link += '/' + this.search.q;
                            }

                            me.permanentLink = link;
                            $rootScope.$broadcast('permanentLinkUpdated', link);
                        });
                    }
                });

                Object.defineProperty(self, 'permanentLink', {value: ''});

                Object.defineProperty(self, 'search', {
                    get: function() {
                        return searchValue;
                    },
                    set: function(newValue) {
                        Object.defineProperty(newValue, 'hasCoordinates', {
                            get: function() {
                                return (this.lat && this.long && this.rad);
                            }
                        });
                        Object.defineProperty(newValue, 'resetCoordinates', {
                            value: function() {
                                this.lat = this.long = this.rad = '';
                            }
                        });

                        searchValue = newValue;
                    }
                });
                Object.defineProperty(self, 'doSearch', {
                    value: function() {
                        Pagination.curPage = 1;
                        $location.path('#/page/1');
                        self.refresh();
                    }
                });

                self.search = searchValue;

                self.refresh();

                return self;
            };

            return new Places();

        }]);

        app.factory('Place', ['$rootScope', '$resource', function($rootScope, $resource) {

            var resource = $resource('/useful-places/API/v1/ngPlace', {},
                {
                    'update': { method: 'PUT' }
                }
            );

            var handleError = function(errorHandler) {
                return function(err) {
                    if (errorHandler) {
                        errorHandler(err);
                    }
                };
            };

            var Place = function(params) {
                if (!params) params = {};

                this.id = params.id ? params.id : null;
                this.name = params.name ? params.name : null;
                this.description = params.description ? params.description : null;
                this.lat = params.lat ? params.lat : null;
                this.long = params.long ? params.long : null;

                Object.defineProperty(this, 'marker', {
                    value: null,
                    writable: true
                });
            };

            Place.get = function(id, successHandler, errorHandler) {
                var place = new Place();

                resource.get({id: id}, function(resp) {
                    place.id = resp.id;
                    place.name = resp.name;
                    place.description = resp.description;
                    place.lat = resp.lat;
                    place.long = resp.long;

                    if (successHandler) {
                        successHandler(place, resp);
                    }
                }, handleError(errorHandler));

                return place;
            };

            Place.prototype.save = function(successHandler, errorHandler) {
                var self = this;

                if (self.id) {
                    resource.update(self, function(resp) {
                        $rootScope.$broadcast('placeUpdated', self);

                        if (successHandler) {
                            successHandler(self, resp);
                        }
                    }, handleError(errorHandler));
                } else {
                    resource.save(self, function(resp) {
                        self.id = resp.id;

                        $rootScope.$broadcast('placeSaved', self);

                        if (successHandler) {
                            successHandler(self, resp);
                        }
                    }, handleError(errorHandler));
                }

                return true;
            };

            Place.prototype.edit = function() {
                $rootScope.$broadcast('placeEdit', this);
            }

            Place.prototype.delete = function(successHandler, errorHandler) {
                var self = this;
                resource.delete({id: this.id}, function(resp) {
                    $rootScope.$broadcast('placeDeleted', self);

                    if (successHandler) {
                        successHandler(self, resp);
                    }
                }, handleError(errorHandler));
            };

            return Place;
        }]);

        app.factory('MapService', ['$rootScope', function($rootScope) {
            var markers = [];

            return {
                map: null,
                markerDfs: {
                    'default': {
                        icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png',
                        shadow: 'https://maps.gstatic.com/mapfiles/ms2/micons/msmarker.shadow.png'
                    },
                    'selected': {
                        icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/yellow-dot.png',
                        shadow: 'https://maps.gstatic.com/mapfiles/ms2/micons/msmarker.shadow.png'
                    },
                    'chosen': {
                        icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/green-dot.png',
                        shadow: 'https://maps.gstatic.com/mapfiles/ms2/micons/msmarker.shadow.png'
                    },
                    'createMarker': {
                        draggable: true,
                        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%20|6991FD',
                        shadow: 'http://chart.apis.google.com/chart?chst=d_map_pin_shadow'
                    },
                    'searchMarker': {
                        draggable: true,
                        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=?|57FC7A',
                        shadow: 'http://chart.apis.google.com/chart?chst=d_map_pin_shadow'
                    },
                    'sizerMarker': {
                        draggable: true,
                        icon: {
                            url: 'http://google.com/mapfiles/kml/paddle/grn-circle-lv.png',
                            scaledSize: new google.maps.Size(10, 10)
                        },
                        shadow: 'http://chart.apis.google.com/chart?chst=d_map_pin_shadow'
                    }
                },
                makeMarker: function(place, type) {
                    if (!type) type = 'default';

                    if (!place) {
                        place = {lat: 0, long: 0};
                    }

                    return new google.maps.Marker(angular.extend({
                        map: this.map,
                        position: new google.maps.LatLng(place.lat, place.long),
                        title: place ? place.name : ''
                    }, this.markerDfs[type]));
                },
                setDefIcon: function(marker) {
                    marker.setIcon(this.markerDfs.default.icon);
                },
                searchMarker: null,
                createMarker: null,
                distanceWidget: null,
                updateTmpMarker: function(tmpMarkerName, latLng, distance) {
                    var self = this;
                    if (!self[tmpMarkerName]) {
                        self[tmpMarkerName] = self.makeMarker(latLng, tmpMarkerName);

                        if (tmpMarkerName == 'searchMarker') {
                            self.distanceWidget = new DistanceWidget({
                                map: self.map,
                                distance: distance,
                                center: self[tmpMarkerName],
                                sizer: self.makeMarker(null, 'sizerMarker')
                            });

                            google.maps.event.addListener(self.distanceWidget, 'distance_changed', function() {
                                self.distanceWidget.distance = self.distanceWidget.get('distance');
                                if (!$rootScope.$$phase) { $rootScope.$apply();}
                            });

                            self.distanceWidget.distance = self.distanceWidget.get('distance');
                        }

                        $rootScope.$broadcast( tmpMarkerName + 'Created', self[tmpMarkerName], self.distanceWidget);
                    }

                    self[tmpMarkerName].setPosition(latLng);
                }
            }
        }]);

        return app;
    }
);
