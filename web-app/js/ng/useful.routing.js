define(['angular'],
    function (angular) {

        var app = angular.module('useful.routing', []);

        app.config(['$routeProvider', function($routeProvider) {
            var placeListRouteConfig = {
                template: ' ',
                controller: 'RouteCtrl'
            };

            $routeProvider
                .when('', placeListRouteConfig)
                .when('/page/:pageId', placeListRouteConfig)
                .when('/page/:pageId/:lat/:long/:rad', placeListRouteConfig)
                .when('/page/:pageId/:q', placeListRouteConfig)
                .when('/page/:pageId/:lat/:long/:rad/:q', placeListRouteConfig)
                .otherwise({redirectTo: '/page/1'});
        }]);

        return app;
    }
);
