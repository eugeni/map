define(['angular'],
    function (angular) {

        var app = angular.module('useful.directives', []);

        app.directive('placeItem', ['MapService', 'places', '$rootScope', function(MapService, places, $rootScope) {
            return {
                restrict: 'A',
                link: function(scope, elem, attrs) {
                    scope.isSelected = false;
                    scope.isOpened = false;
                    scope.isChosen = false;

                    if (!scope.marker) {
                        scope.marker = MapService.makeMarker(scope.place);
                    }

                    scope.$on('$destroy', function() {
                        if (!scope.marker) return;

                        scope.marker.setMap(null);
                    });

                    scope.mouseOver = function() {
                        scope.isSelected = true;
                        if (!scope.isChosen) {
                            scope.marker.setIcon(MapService.markerDfs.selected.icon);
                        }
                    };
                    scope.mouseOut = function() {
                        if (!scope.isChosen) {
                            MapService.setDefIcon(scope.marker);
                        }
                        scope.isSelected = false;
                    };

                    scope.uiEvents = {
                        'map-mouseover': 'mouseOver(true)',
                        'map-mouseout': 'mouseOut(true)',
                        'map-click': 'mouseClick(true)',
                        'map-dblclick': 'edit(true)'
                    };

                    scope.$on('closePlaces', function(evnt, currentOpenedPlaceId) {
                        if (scope.place.id == currentOpenedPlaceId) return;

                        scope.isOpened = false;
                        scope.isChosen = false;
                        MapService.setDefIcon(scope.marker);
                    });

                    scope.mouseClick = function(isMapEvent) {
                        scope.marker.setIcon(MapService.markerDfs.chosen.icon);
                        scope.isChosen = true;
                        $rootScope.$broadcast('closePlaces', scope.place.id);

                        if (isMapEvent) {
                            if (!scope.infoWindow) {
                                scope.infoWindow = new google.maps.InfoWindow({
                                    content: scope.place.name + '<br>' + scope.place.description
                                })
                            }
                            scope.infoWindow.open(MapService.map, scope.marker);
                        } else {
                            scope.isOpened = !scope.isOpened;
                        }
                    };

                    scope.$on('unChooseMarkers', function() {
                        scope.isChosen = false;
                        MapService.setDefIcon(scope.marker);
                    });

//                    Control buttons
                    scope.zoomTo = function($event) {
                        $event.stopPropagation();

                        MapService.map.panTo(scope.marker.position);
                    };
                    scope.edit = function($event) {
                        if ($event) $event.stopPropagation();

                        scope.place.edit();

                        MapService.updateTmpMarker('createMarker', new google.maps.LatLng(scope.place.lat, scope.place.long));
                    };
                    scope.delete = function($event) {
                        $event.stopPropagation();

                        scope.place.delete(function(place) {
                            places.refresh();
                        });
                    }
                }
            }
        }]);


        return app;
    }
);
