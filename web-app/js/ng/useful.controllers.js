define(['angular'],
    function (angular) {

        var app = angular.module('useful.controllers', []);

        app.controller('RouteCtrl', ['$routeParams', 'Pagination', '$rootScope', 'MapService', 'places',
            function($routeParams, Pagination, $rootScope, MapService, places) {
                Pagination.curPage = $routeParams.pageId ? $routeParams.pageId : 1;
                if ($routeParams.q) {
                    places.search.q = $routeParams.q;
                }
                if ($routeParams.lat && $routeParams.long && $routeParams.rad) {
                    places.search.lat = $routeParams.lat;
                    places.search.long = $routeParams.long;
                    places.search.rad = $routeParams.rad;

                    MapService.updateTmpMarker(
                        'searchMarker',
                        new google.maps.LatLng(places.search.lat, places.search.long),
                        places.search.rad
                    );
                    MapService.map.panTo(new google.maps.LatLng(places.search.lat, places.search.long));
                }
                places.refresh();
            }
        ]);

        app.controller('PaginationCtrl',
            ['$scope', 'Pagination',
                function ($scope, Pagination) {
                    $scope.data = Pagination;
                }
            ]);

        app.controller('PlaceListCtrl',
            ['$scope', 'places',
                function ($scope, places) {
                    $scope.places = places;
                }
            ]);

        app.controller('MapCtrl',
            ['$scope', 'MapService', '$rootScope',
                function ($scope, MapService, $rootScope) {
                    $rootScope.createMarker = null;

                    $scope.mapOptions = {
                        center: new google.maps.LatLng(49, 31),
                        zoom: 6,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    $scope.$watch('upMap', function(newValue) {
                        MapService.map = newValue;
                    });

                    $scope.earthMoved = function() {
                        if (MapService.searchMarker) {
                            $rootScope.searchMarkerInvisible = !MapService.map.getBounds().contains(MapService.distanceWidget.position);
                        }
                        if (MapService.createMarker) {
                            $rootScope.createMarkerInvisible = !MapService.map.getBounds().contains(MapService.createMarker.position);
                        }
                    };

                    $scope.unChooseMarkers = function() {
                        $rootScope.$broadcast('unChooseMarkers');
                    };

                    $scope.mapEvents = {
                        'map-click': 'unChooseMarkers()',
                        'map-center_changed': 'earthMoved()'
                    };
                }
            ]);

        app.controller('SearchCtrl',
            ['$scope', 'MapService', 'Pagination', 'places',
                function ($scope, MapService, Pagination, places) {
                    $scope.$on('searchMarkerCreated', function(evnt, sMarker, dWidget) {
                        $scope.searchMarker = sMarker;
                        $scope.distanceWidget = dWidget;
                    });

                    $scope.$on('permanentLinkUpdated', function(evnt, newPermanentLink) {
                        $scope.permanentLink = newPermanentLink;
                    });

                    $scope.data = places.search;

                    function updatePosition(newVal, fn) {
                        if (!newVal) {
                            $scope.delMarker(true);
                            return;
                        }

                        if (!MapService.distanceWidget && $scope.data.lat && $scope.data.long && $scope.data.rad) {
                            if ($scope.timer) {
                                clearTimeout($scope.timer);
                            }
                            $scope.timer = setTimeout(function() {
                                MapService.updateTmpMarker('searchMarker', new google.maps.LatLng($scope.data.lat, $scope.data.long), $scope.data.rad);
                            }, 750);

                            return;
                        } else if (!MapService.distanceWidget) {
                            return;
                        }
                        if ($scope.timer) {
                            clearTimeout($scope.timer);
                        }
                        $scope.timer = setTimeout(function() {
                            if (!MapService.distanceWidget) return;
                            fn();
                        }, 750);
                    }

                    $scope.$watch('data.lat', function(newVal) {
                        updatePosition(newVal, function() {
                            var oldPosition = MapService.distanceWidget.position;
                            MapService.updateTmpMarker('searchMarker', new google.maps.LatLng(newVal, oldPosition.lng()));
                        });
                    });
                    $scope.$watch('data.long', function(newVal) {
                        updatePosition(newVal, function() {
                            var oldPosition = MapService.distanceWidget.position;
                            MapService.updateTmpMarker('searchMarker', new google.maps.LatLng(oldPosition.lat(), newVal));
                        });
                    });
                    $scope.$watch('data.rad', function(newVal) {
                        updatePosition(newVal, function() {
                            MapService.distanceWidget.setDistance(newVal);
                        });
                    });

                    $scope.addMarker = function($event, $params) {
                        var mapCenter = MapService.map.getCenter();

                        MapService.updateTmpMarker('searchMarker', mapCenter);
                    };

                    $scope.delMarker = function(withoutSearchReset) {
                        if ($scope.searchMarker) {
                            $scope.searchMarker.setMap(null);
                            $scope.searchMarker = null;
                            $scope.distanceWidget = null;
                            $scope.searchMarkerInvisible = false;

                            MapService.searchMarker = null;
                            MapService.distanceWidget = null;
                        }

                        if (withoutSearchReset) return;

                        $scope.data.resetCoordinates();
                    };

                    $scope.doSearch = places.doSearch;

                    $scope.panTo = function() {
                        MapService.map.panTo(MapService.distanceWidget.position);
                    };

                    $scope.$watch('distanceWidget.position', function(newVal) {
                        if (!newVal) return;

                        places.search.lat = newVal.lat();
                        places.search.long = newVal.lng();
                    });

                    $scope.$watch('distanceWidget.distance', function(newVal) {
                        if (!newVal) return;

                        places.search.rad = newVal;
                    });
                }
            ]);

        app.controller('CreatePlaceCtrl',
            ['$scope', 'MapService', 'Place', 'places',
                function ($scope, MapService, Place, places) {
                    $scope.place = new Place();

                    $scope.$on('createMarkerCreated', function(evnt, cMarker) {
                        $scope.createMarker = cMarker;
                    });

                    $scope.$watch('createMarker.position', function(newVal) {
                        if (!newVal) return;

                        $scope.place.lat = newVal.lat();
                        $scope.place.long = newVal.lng();
                    });

                    $scope.delMarker = function(withoutPlaceReset) {
                        if ($scope.createMarker) {
                            $scope.createMarker.setMap(null);
                            $scope.createMarker = null;
                            $scope.createMarkerInvisible = false;

                            MapService.createMarker = null;
                        }

                        if (withoutPlaceReset) return;

                        $scope.place = new Place();
                    };

                    $scope.addMarker = function($event, $params) {
                        var mapCenter = MapService.map.getCenter();

                        MapService.updateTmpMarker('createMarker', mapCenter);
                    };

                    $scope.saveMarker = function() {
                        $scope.place.save(function(place) {
                            $scope.delMarker();
                        });
                    };

                    $scope.panTo = function() {
                        MapService.map.panTo(MapService.createMarker.position);
                    };

                    $scope.$on('placeEdit', function(event, place) {
                        $scope.place = place;
                    });

                    $scope.$on('placeSaved', function(event, place) {
                        places.splice(0, 0, place);
                    });

                    $scope.$on('placeUpdated', function(event, place) {
                        places.refresh();
                    });

                    function updatePosition(newVal, fn) {
                        if (!newVal) {
                            $scope.delMarker(true);
                            return;
                        }

                        if (!MapService.createMarker && $scope.place.lat && $scope.place.long) {
                            if ($scope.timer) {
                                clearTimeout($scope.timer);
                            }
                            $scope.timer = setTimeout(function() {
                                MapService.updateTmpMarker('createMarker', new google.maps.LatLng($scope.place.lat, $scope.place.long));
                            }, 750);

                            return;
                        } else if (!MapService.createMarker) {
                            return;
                        }
                        if ($scope.timer) {
                            clearTimeout($scope.timer);
                        }
                        $scope.timer = setTimeout(function() {
                            if (!MapService.createMarker) return;
                            fn();
                        }, 750);
                    }

                    $scope.$watch('place.lat', function(newVal) {
                        updatePosition(newVal, function() {
                            var oldPosition = MapService.createMarker.position;
                            MapService.updateTmpMarker('createMarker', new google.maps.LatLng(newVal, oldPosition.lng()));
                        });
                    });
                    $scope.$watch('place.long', function(newVal) {
                        updatePosition(newVal, function() {
                            var oldPosition = MapService.createMarker.position;
                            MapService.updateTmpMarker('createMarker', new google.maps.LatLng(oldPosition.lat(), newVal));
                        });
                    });
                }
            ]);

        return app;
    }
);
