package org.useful.search

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.index.query.FilterBuilders
import org.elasticsearch.index.query.QueryBuilders

class SearchService {
    GrailsApplication grailsApplication
    ElasticSearchClientFactory elasticSearchClientFactory

    private List<String> indexTypes

    private List<String> getIndexTypes() {
        log.debug("indexTypes: " + this.indexTypes)
        if (this.indexTypes) return this.indexTypes

        List<String> domainList = grailsApplication.getArtefacts("Domain")*.clazz.canonicalName
        List<String> indexTypes = []
        domainList.each { domainClassName ->
            println('domainClassName: ' + domainClassName)

            Class domainClass = grailsApplication.getDomainClass(domainClassName).clazz
            if (domainClass.getDeclaredFields().name.contains('searchable')
                    && domainClass.searchable.size()) {
                indexTypes.add(domainClassName)
            }
        }
        this.indexTypes = indexTypes
        log.debug("indexTypes: " + this.indexTypes)
        return this.indexTypes
    }

    public def search(String fullText, Double lat, Double lon, Double rad, Map pagination) {
        log.debug('Search')
        log.debug('Ful text: ' + fullText)
        log.debug('Lat: ' + lat)
        log.debug('Lon: ' + lon)
        log.debug('radius: ' + rad)

        String indexName = grailsApplication.config.up.elasticsearch.index.name
        log.debug("indexName: " + indexName)

        SearchRequestBuilder searchRequestBuilder = elasticSearchClientFactory.elasticSearchClient
                .prepareSearch(indexName)
        this.getIndexTypes().each { type ->
            searchRequestBuilder.setTypes(type)
        }

        if (fullText) {
            searchRequestBuilder.setQuery(QueryBuilders.queryString(fullText))
        }

        if (lat && lon && rad) {
            searchRequestBuilder.setFilter(
                    FilterBuilders.geoDistanceFilter("location")
                            .distance("${rad}m")
                            .lat( lat)
                            .lon(lon)
            )
        }

        if (pagination.offset) {
            searchRequestBuilder.setFrom(pagination.offset)
        }
        if (pagination.max) {
            searchRequestBuilder.setSize(pagination.max)
        }

        SearchResponse response = searchRequestBuilder
                .execute()
                .actionGet();

        List<Object> res = []
        response.internalResponse.hits.hits.each { hit ->
            res.add(grailsApplication.getDomainClass(hit.type).clazz.get(hit.id.toLong()))
        }

        return [
                results: res,
                totalCount: response.internalResponse.hits.totalHits
        ]
    }
}
