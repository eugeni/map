package org.useful.search

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.elasticsearch.action.delete.DeleteResponse
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.common.xcontent.XContentBuilder

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder

class IndexingService {
    GrailsApplication grailsApplication
    ElasticSearchClientFactory elasticSearchClientFactory

    public void createUpdate(Object obj) {
        log.debug('CreateUpdate index: ' + obj.id)

        String indexName = grailsApplication.config.up.elasticsearch.index.name
        log.debug("indexName: " + indexName)

        String indexType = obj.getClass().canonicalName
        log.debug("indexType: " + indexType)

        log.debug('Creating of jsonBuilder')
        XContentBuilder jsonBuilder = jsonBuilder()
                .startObject()
        if (obj.searchable && obj.searchable instanceof Collection) {
            obj.searchable.each { String fieldName ->
                if ("location" == fieldName) {
                    jsonBuilder.field(fieldName, obj[fieldName].reverse())
                } else {
                    jsonBuilder.field(fieldName, obj[fieldName])
                }

                log.debug "${fieldName} : ${obj[fieldName]}"
            }
        } else {
            log.error("Object hasn't 'searchable' parameter or this parameter incorrect.")
        }
        jsonBuilder.endObject()

        log.debug('Creating of index')
        IndexResponse response = elasticSearchClientFactory.elasticSearchClient
                .prepareIndex(indexName, indexType, obj.id.toString())
                .setSource(jsonBuilder)
                .execute()
                .actionGet()

        log.debug('Index instance version: ' + response.getVersion())
    }

    public void delete(Object obj) {
        log.debug('Delete index: ' + obj.id)

        String indexName = grailsApplication.config.up.elasticsearch.index.name
        log.debug("indexName: " + indexName)

        String indexType = obj.getClass().canonicalName
        log.debug("indexType: " + indexType)

        DeleteResponse response = elasticSearchClientFactory.elasticSearchClient
                .prepareDelete(indexName, indexType, obj.id.toString())
                .execute()
                .actionGet();

        log.debug("isNotFound: " + response.isNotFound())
    }
}
