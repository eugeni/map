<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>

    <script type="text/javascript"src="${resource(dir: "js", file: 'require.js')}"></script>
    <script type="text/javascript">
        requirejs.config({
            baseUrl: "${grailsApplication.config.grails.serverURL}/js",
            paths: {
                async: 'async'
            },
            shim: {
                'backbone': {
                    deps: ['underscore', 'jquery'],
                    exports: 'Backbone'
                },
                'underscore': {
                    exports: '_'
                },
                'backbone.paginator': {
                    deps: ['backbone', 'underscore', 'jquery'],
                    exports: 'Paginator'
                }
            }
        });
    </script>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'useful-places.main.js')}" ></script>

    <r:require modules="useful_places_main"/>

</head>
<body>
    <div class="useful-places-block">
        <div class="useful-places">
            <div>
                <g:hiddenField name="id"/>
                <g:textField name="name" placeholder="Title"/>
                <g:textField name="description" placeholder="Description"/>
                <g:textField name="lat" placeholder="Latitude"/>
                <g:submitButton name="getPoint" id="getPointButton" type="button" value="Get"/>
                <g:textField name="long" placeholder="Longitude"/>
                <g:submitButton name="Save" id="saveButton" type="button"/>
                <g:submitButton name="Reset" id="resetButton" type="button"/>
                <g:submitButton name="Refresh" id="refresh" type="button"/>
            </div>

            <ul id="placeList"></ul>
        </div>
        <div id="pagination">

        </div>
    </div>

    <div class="right-side">
        <div id="search-form">
            <g:textField name="search" placeholder="Searched text"/><br>
            <g:textField name="searched-lat" placeholder="Latitude"/>
            <g:textField name="searched-long" placeholder="Longitude"/>
            <g:submitButton name="getCenterPoint" id="getCenterPointButton" type="button" value="Get center"/>
            <g:textField name="searched-rad" placeholder="Radius"/>
            <g:submitButton name="Search" id="searchButton" type="button"/>
            <g:submitButton name="Reset" id="resetButton" type="button"/>
        </div>

        <div id="map-canvas"></div>
    </div>

<script id="placeTmpl" type="text/template">
    <div class="view">{{id}} - {{name}} <button type="button" class="destroy">X</button></div>
</script>

<script id="paginationTmpl" type="text/template">
        <ul>
            {{#pagination}}
                <li class="{{current}}"><a href="#">{{n}}</a></li>
            {{/pagination}}
        </ul>
</script>
</body>
</html>
