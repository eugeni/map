<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
    <script type="text/javascript"src="${resource(dir: "js", file: 'require.js')}"></script>
    <script type="text/javascript">
        requirejs.config({
            baseUrl: "${grailsApplication.config.grails.serverURL}/js/ng",
            paths: {
                'ui-map': 'ui-map',
                async: '../async'
            },
            shim: {
                'angular': { exports: 'angular' },
                'ui-map': {
                    deps: [
                        'event',
                        'async!https://maps.googleapis.com/maps/api/js?key=AIzaSyCdE6ucBkIhEh40pRJYid_ZLkJse10xcPA&sensor=false'
                    ]
                },
                'event': {
                    deps: [ 'angular' ]
                },
                'angular-resource': {
                    deps: [ 'angular' ]
                },
                'search.point.widget': {
                    deps: [
                        'async!https://maps.googleapis.com/maps/api/js?key=AIzaSyCdE6ucBkIhEh40pRJYid_ZLkJse10xcPA&sensor=false'
                    ]
                }
            }
        });
    </script>
    <script type="text/javascript"src="${resource(dir: "js/ng", file: 'main.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css/ng', file: 'main.css')}" type="text/css">
</head>
<body>


    <div id='usefulPlacesMap' ng-non-bindable class="container" ng-cloak>
        <div ng-view></div>

        <div class="search-form" ng-controller="SearchCtrl">
            <form ui-map-marker="searchMarker" search-point>
                <label for="search">Search:</label><g:textField name="search" ng-model="data.q"/>
                <button type="button" ng-click="doSearch()">Search</button>
                <br>

                <label for="searched-lat">Lat:</label><g:textField name="searched-lat" ng-model="data.lat"/>
                <label for="searched-long">Lng:</label><g:textField name="searched-long" ng-model="data.long"/>
                <label for="searched-rad">Radius:</label><g:textField name="searched-rad" ng-model="data.rad"/>
                <button type="button" ng-click="addMarker()">Set point</button>
                <button type="button" ng-click="delMarker()">Del point</button>
                <button type="button" ng-click="panTo()" ng-class="{invisible: !searchMarkerInvisible}">><</button>
            </form>

            <div class="permanent-link">
                <a href="{{permanentLink}}">Permanent link</a>
            </div>
        </div>

        <hr>

        <div class="place-list">
            <form ui-map-marker="createMarker" ng-controller="CreatePlaceCtrl">
                <label for="name">Name:</label><g:textField name="name" ng-model="place.name"/>
                <label for="description">Description:</label><g:textField name="description" ng-model="place.description"/>
                <label for="lat">Lat:</label><g:textField name="lat" ng-model="place.lat"/>
                <label for="lng">Lng:</label><g:textField name="lng" ng-model="place.long"/>
                <button type="button" ng-click="addMarker()">Set point</button>
                <button type="button" ng-click="delMarker()">Del point</button>
                <button type="button" ng-click="saveMarker()">Save</button>
                <button type="button" ng-click="panTo()" ng-class="{invisible: !createMarkerInvisible}">><</button>
            </form>

            <br>

            <div ng-controller="PlaceListCtrl">
                <ul id="placeList">
                    <li ng-repeat="place in places" place-item ng-class="{current: isSelected, opened: isOpened, chosen: isChosen}"
                        ng-mouseenter="mouseOver()" ng-mouseleave="mouseOut()" ng-click="mouseClick()"
                        ui-map-marker="marker" ui-event="uiEvents" >
                            {{place.id}} - {{place.name}}
                            <div class="description">{{place.description}}</div>
                            <a href="" class="zoom-to" ng-click="zoomTo($event)">><</a>
                            <a href="" class="edit" ng-click="edit($event)">E</a>
                            <a href="" class="delete" ng-click="delete($event)">D</a>
                    </li>
                </ul>
            </div>

            <ul class="place-pagination" ng-controller="PaginationCtrl">
                <li ng-repeat="page in [] | pageRange">
                    <a ng-class="{current: data.curPage == page}" href="#/page/{{page}}">{{page}}</a>
                </li>
            </ul>
        </div>

        <div class="map-area" ng-controller="MapCtrl">
            <div id="map_canvas" ui-map="upMap" ui-options="mapOptions" ui-event="mapEvents"></div>
        </div>
    </div>


</body>
</html>
