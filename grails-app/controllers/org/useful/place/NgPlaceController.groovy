package org.useful.place

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import org.useful.search.SearchService

class NgPlaceController {
    SearchService searchService

    def show() {
        println('show')

        if (!params.int('max')) {
            params.max = 10
        }
        if (!params.int('offset')) {
            params.offset = 0
        }

        Map listParams = [
                max: params.int('max'),
                offset: params.int('offset')
        ]

        if (params.q || (params.lat && params.long && params.rad)) {
            Map searchResult = searchService.search(params.q, params.double('lat'), params.double('long'), (params.double('rad') ?: 0) * 1000, listParams)
            render ([
                    'data': searchResult.results.collect { it.toMap() },
                    'total': searchResult.totalCount
            ] as JSON)
            return
        }

        PlaceGeo place
        if (params.id && (place = PlaceGeo.get(params.id))) {
            render (place.toMap() as JSON)
            return
        }

        render ([
                'data': PlaceGeo.list(listParams).collect { it.toMap() },
                'total': PlaceGeo.count()
        ] as JSON)
    }

    def save() {
        println('save')

        if (!params.double('lat') || !params.double('long') || !params.name) {
            render(status: 400)
            return
        }

        PlaceGeo place = new PlaceGeo(
                name: params.name,
                description: params.description,
                location: [params.double('lat'), params.double('long')]
        )

        if (!place.save()) {
            render(status: 400) //422
            return
        }

        render (place.toMap() as JSON)
    }

    def update() {
        println('update')

        PlaceGeo place = PlaceGeo.get(params.id)
        if (!place) {
            render(status: 400) //404
            return
        }

        if (params.name) {
            place.name = params.name
        }
        if (params.description) {
            place.description = params.description
        }

        List location = place.location
        if (params.double('lat')) {
            location[0] = params.double('lat')
        }
        if (params.double('long')) {
            location[1] = params.double('long')
        }
        place.location = location

        if (!place.save()) {
            render(status: 400) //422
            return
        }

        render (place.toMap() as JSON)
    }

    def delete() {
        println('delete')

        PlaceGeo place = PlaceGeo.get(params.long('id'))
        if (!place) {
            render(status: 400) //404
            return
        }

        try {
            place.delete()
            render ([:] as JSON)
        } catch(DataIntegrityViolationException e) {
            render(status: 400) //422
        }
    }
}
