package org.useful.place

import org.useful.search.IndexingService

class PlaceGeo {
    IndexingService indexingService

    static List<String> searchable = [
            "name",
            "description",
            "location"
    ]

    static transients = ['indexingService']

    static mapWith = "mongo"

    String name
    String description
    List location

    static mapping = {
        location geoIndex:true
    }

    static constraints = {
        name nullable: false, blank: false
        description nullable: true, blank: false
    }

    def afterInsert() {
        indexingService.createUpdate(this)
    }

    def afterUpdate() {
        indexingService.createUpdate(this)
    }

    def afterDelete() {
        indexingService.delete(this)
    }

    Map toMap() {
        return [
                id: id,
                name: name,
                description: description,
                lat: location[0],
                long: location[1]
        ]
    }
}
