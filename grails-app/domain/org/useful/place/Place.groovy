package org.useful.place

class Place {

    String name
    String description
    Long locationId

    static constraints = {
        name nullable: false, blank: false
        description nullable: true, blank: false
        locationId nullable: false
    }

    static transients = ['location']

    List getLocation() {
        PlaceGeo placeGeo = PlaceGeo.get(locationId)
        if (placeGeo) {
            return placeGeo.location
        } else {
            return []
        }
    }

    void setLocation(List location) {
        PlaceGeo placeGeo = PlaceGeo.get(locationId)
        if (!placeGeo) {
            placeGeo = new PlaceGeo(location: location)
        }

        placeGeo.save()
        locationId = placeGeo.id
    }


    Map toMap() {
        return [
                id: id,
                name: name,
                description: description,
                lat: location[0],
                long: location[1]
        ]
    }
}
