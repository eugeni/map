import org.useful.search.ElasticSearchClientFactory

// Place your Spring DSL code here
beans = {
    elasticSearchClientFactory(ElasticSearchClientFactory) {
        grailsApplication = ref('grailsApplication')
    }
}
