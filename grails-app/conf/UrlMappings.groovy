class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}


        "/place"(view: 'place/index')
        "/API/v1/place/$id?"(resource: "place")
        "/API/v1/place/$id"(controller: "place", parseRequest: true) {
            action = [GET: "show", PUT: "update", DELETE: "delete", POST: "save"]
        }

        "/ngPlace"(view: 'ngPlace/index')
        "/API/v1/ngPlace/$id?"(resource: "ngPlace")
        "/API/v1/ngPlace/$id"(controller: "ngPlace", parseRequest: true) {
            action = [GET: "show", PUT: "update", DELETE: "delete", POST: "save"]
        }


		"/"(view:"/index")
		"500"(view:'/error')
	}
}
